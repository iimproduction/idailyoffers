<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

//update_option('siteurl','http://ido.iimdevelopment.net');
//update_option('home','http://ido.iimdevelopment.net');
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'developer');

/** MySQL database username */
define('DB_USER', 'developer');

/** MySQL database password */
define('DB_PASSWORD', 'developer');

/** MySQL hostname */
define('DB_HOST', 'developer');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'p5C pS<<l6|4F+HpCw<+nt4|]MRPP>Rx. opU~x{CO-|_maL,T |?,[-TpmwD5[B');
define('SECURE_AUTH_KEY',  'C^HxWb>Vpiv(AXPjWuCVjgIR+3aid_+RG%M<l-8TJ{jDe|{1_-Y/m>]0`{Q2<u[m');
define('LOGGED_IN_KEY',    '<EB4W|OsE@S0jJp6()JgK[DG+r&6?3x`F.O7&nC-8!BQDWGBxZ^eAxpXo+|@V;Ny');
define('NONCE_KEY',        ':oVd+?X9E5ng#X-pZkZq-{ZP@?+L_?=hOhxz-^%x8rW;6@I)%Y-<pf*O&=eEv0d;');
define('AUTH_SALT',        '7 hA+jE}6V k<T}g}77AtaN9m/jAC2e.!%}E]gb4_cGXXNYuXP:9!)@@IVfQ,=~U');
define('SECURE_AUTH_SALT', '=QSIXku]hB(GwLWOhKe!%UYc#@|!GQ/SYP-/jUe e?k7671|#&b~c({&=v<?L3`%');
define('LOGGED_IN_SALT',   '+@mXSM}s{A/`P!i!~Z}C>sF&*~}!SZyiz}BsO}8-A#Oy-U^m |tZq+Q?l027jFxa');
define('NONCE_SALT',       'CxZ~(|/>86.u@SF^u,7G2u7y*T&>Ar@lK#Rc2KH4jZ(-F~+|tv~|?7S6PJOfB#u+');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

define('RELOCATE',true);
/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
