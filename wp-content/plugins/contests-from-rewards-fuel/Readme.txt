===Contests by Rewards Fuel===
Contributors: Rewards Fuel
Tags: contests, giveaways, sweepstakes, Facebook, Instagram, Twitter, Share, Newsletter signup, MailChimp, Awber, comments,campaign, competitions, contest,  dashboard, easy, embed, encourage subscriptions, engagement, entry form, facebook campaign, facebook contest, facebook events, facebook fanpages, facebook promotion, instagram contests, Increase twitter followers, Increase Instagram followers, Increase facebook followers, integration, manage, marketing, multiple platforms, photo contests, promote, promotion, promotions, referrals, shortcode, simple, social, social campaign, Social campaigns, social contests, social media sharing, social promotion, social promotions, sweepstake, sweepstakes, twitter, widget, widgets, wordpress, wordpress comments, Wordpress contest
Donate link: http://rewardsfuel.com/pricing.php
Requires at least: 3.0.1
Tested up to: 4.3.1
Stable tag: 1.0.7
License: MIT
License URI: http://RewardsFuel.com/wordpress/license.txt

Create contests that compel your visitors to subscribe to your newsletter, follow on Instagram / Twitter, Retweet, Comment, like on Facebook and more.

==Description==
Contests by Rewards Fuel lets you run contests that will help you earn newsletter subscribers, Instagram & Twitter Followers, blog comments, Retweets, Facebook Likes, and more.  Use this plugin to create and display contests right from your WordPress website.

Choosing the right entry method(s) is a great way to offer incentive to encourage specific actions from your audience.
= Entry methods currently supported =
1. Follow on Instagram entry,
2. #Tag on Instagram - Enter directly on Instagram and show the results on your page with voting coming soon,
3. Subscribe to a newsletter entry - CSV Export- MailChimp automatic export and Aweber automatic export,
4. Follow on Twitter entry includes [automatic entry](http://RewardsFuel.com/features.php#auto_entry "What's automatic entry?"),
5. Suggested like for Facebook Pages similar to forcing someone to like a page first,
6. RSVP to a Facebook event entry includes [automatic entry](http://RewardsFuel.com/features.php#auto_entry "What's automatic entry?"),
7. Share to enter - every person that shares your contest and encourages more entries receives more entry points,
8. Comment to enter - only works with the default WordPress comments,
9. Retweet entry - includes [automatic entry](http://RewardsFuel.com/features.php#auto_entry "What's automatic entry?"),
10. Reply to a Tweet entry - includes [automatic entry](http://RewardsFuel.com/features.php#auto_entry "What's automatic entry?"),
11. Basic entry - Enter directly on your site,
12. More entry methods are being created every month let us know if there's a method you would like to see added.

= Simple contest creation and display =
Start your first contest in minutes, simply answer a few questions about when you want your contest to run, what you're giving away, and how you want people to enter.  We will give you a short code like [RF_CONTEST contest='1234'] that you can place  anywhere in your post where you want the contest to appear  - just like a YouTube video.

= Features =
1. Several responsive contest layouts - Choose the layout and color that aligns with your brand. All contests respond perfectly from mobile to desktops,
2. Easy embed text - Place the contest anywhere in your blog post, no need for coding,
3. Verified entry methods - We make sure your visitors complete your actions e.g. we check to make sure contestants entered and continue to monitor for the entire contest,
4. Statistics - See who entered your contest, including geographic and demographic information,
5. Automation - Schedule your contest from start to finish on specific dates,
6. Integrations - We connect to your accounts to save you time.

= It's FREE =
To use this plugin you will need to sign up for a free Rewards Fuel account, no credit card or trial period needed.  You will get instant access to our contest creation software as well as several free features,  plus the option to purchase additional paid features  if required or needed.


== Installation ==
* Upload the folder to the /wp-content/plugins/ directory,
* Activate the plugin through the 'Plugins' menu in WordPress,
* You will now have an additional "Contests by Rewards Fuel" box under your post editor for adding your contests,
* We have included a settings page under the settings menu which includes some instructions

== Frequently Asked Questions ==
= Is this free =
Yes we have a 100% free version which is fully functional, no trial period.

= Do I have to sign up for Rewards Fuel to use this =
Yes- it's free and takes a few seconds

= Do I need a credit card to sign up =
No!

= What is my API Key & how do I get it =
Your API Key is your Rewards Fuel tracking number- it allows us to display information that only you should see e.g. all your contests with their embed code and in the latest version we have made it easier to install by just activating your Rewards Fuel Account with one button.

= How to I put a contest into a post =
There is a box that appears on your Add Post & Edit Post pages, once you add your API key you should see a list of your contests you have created.  Select the contest you want to display, copy the text that looks like this [RF_CONTEST contest='1234']  place that wherever you want your contest to appear.

= What is AUTOMATIC ENTRY =
Automatic entry allows you to determine an event you want such as Twitter user sending a Retweet and Rewards Fuel will watch for that event to take place and automatically enter that person for you.  In addition we can contact that person (if you choose) and notify them that they are entered and that they can improve their odds of winning by visiting your contest page (encouraging more entries).  This works great to turn a Retweet entry into a new newsletter subscriber.

= Something we missed =
Please email us info@rewardsfuel.com


==Screenshots==
1. This is what it looks like to create a contest.
2. After you create a contest you will get a short code like [RF_CONTEST contest='1234'].
3. Copy and paste your short code anywhere you would like a contest to appear.
4. Your contest will appear in your blog posts (there are several different layouts).
5. Here is another layout.
6. And one more layouts with multiple background images.
7. When someone enters your contest we verify they followed, liked, or Retweeted for you.
8. Use WordPress comments as an entry method.

==Changelog==
V 1.0.8 - We've made it easier to complete the activation and get started on creating your first contest by making the API Key less of a task.

==Upgrade Notice==
We've made it easier to complete the activation and get started on creating your first contest by making the API Key less of a task.